/*
  BlueTasks Config for Netlify Compilation
*/
module.exports = {
  client: '[client name]',
  node_env: 'production',
  tasks: ['clean', 'copy', 'pug', 'sass', 'script', 'script_legacy', 'vue'],
  sass: {
    options: {
      minify: true,
    },
  },
  vue: {
    options: {
      minify: true,
    },
  },
  script: {
    options: {
      minify: true,
    },
  },
};
