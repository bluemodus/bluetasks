import Vue from 'vue'
import Router from 'vue-router'

export default class {
	constructor() {
		Vue.use(Router)

		this.router = new Router({
			routes: []
		});
	}
}
