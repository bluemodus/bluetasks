import Vue from 'vue'
import Vuex from 'vuex'

export default class {
	constructor() {
		Vue.use(Vuex);
		const state = {};
		const getters = {};
		const mutations = {};
		const actions = {};

		this.store = new Vuex.Store({
			state,
			getters,
			mutations,
			actions
		});
	}
}
