/*-- import aliases -----------
'!/...' -> project root
'~/...' -> vue folder root
'@/...' -> src folder root
-----------------------------*/

import Vue from 'vue';
import Store from '@/myapp/store';
import Router from '@/myapp/router';
import App from '@/myapp/App';
import Blue from './../js/core';

Vue.config.devtools = true;
Vue.config.productionTip = false;

new Blue(
  {
    name: 'My Vue App',
    selector: '.myapp',
    setup(el) {
      const app = new Vue({
        store: new Store().store,
        router: new Router().router,
        data: () => Object.assign({}, el.dataset),
        render: h => h(App),
      });
      app.$mount(el);
    },
  },
  false
);
