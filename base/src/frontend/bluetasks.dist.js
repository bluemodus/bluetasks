/*
  BlueTasks Config for Backend Compilation
*/
module.exports = {
  client: '[client name]',
  node_env: 'production',
  output: './',
  tasks: ['clean', 'copy', 'sass', 'script', 'vue'],
  copy: {
    output: './src/Xp13Boilerplate.Web/',
  },
  sass: {
    output: './src/Xp13Boilerplate.Web/styles/',
    options: {
      minify: true,
    },
  },
  script: {
    output: './src/Xp13Boilerplate.Web/js/',
    options: {
      minify: true,
    },
  },
  vue: {
    output: './src/Xp13Boilerplate.Web/',
    output_js: '/js/',
    output_css: '/styles/',
  },
};
