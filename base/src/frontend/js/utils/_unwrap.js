export default function(el) {
  const wrap = el.parentElement;
  const parent = wrap.parentElement;
  while (wrap.firstChild) {
    parent.insertBefore(wrap.firstChild, wrap);
  }
  parent.removeChild(wrap);
}
