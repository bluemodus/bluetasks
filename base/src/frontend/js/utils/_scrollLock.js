import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';

const scrollLock = {
	lock(target, options = {}) {
		disableBodyScroll(target, options);
	},
	unlock(target, options = {}) {
		enableBodyScroll(target, options);
	},
	clear(options = {}) {
		clearAllBodyScrollLocks(options);
	}
};

export default scrollLock