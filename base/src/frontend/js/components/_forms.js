import Blue from "~/core";

new Blue({
	name: 'Form Validate',
	selector: ".form--validate",
	setup(form) {
		this.$validate(form);
	}
});