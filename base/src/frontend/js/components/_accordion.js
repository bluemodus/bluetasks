import Blue from "~/core";

new Blue({
	name: 'Accordion',
	selector: ".accordion",
	setup(accordion) {
    let trigger = accordion.querySelectorAll('.accordion__trigger')
    let content = accordion.querySelectorAll('.accordion__content')
		let config = {
			trigger,
			content
		}
    new this.$toggle(config)
	}
});