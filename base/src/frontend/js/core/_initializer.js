export default class Initializer {
  static #HTML = document.querySelector("html");
  static get methods() {
    for (let method in Initializer) {
      console.log(`["${method}"]()`);
    }
  }

  #addMethod(instance) {
    if (this.hasOwnProperty(instance.$name)) {
      console.error(
        `The setup method "${instance.$name}" has already been defined. This value must be unique.`
      );
    } else {
      Object.assign(Initializer, {
        [instance.$name]: () => {
          instance.$elements.forEach(element => {
            if (!element._init) {
              element._init = [];
            }
            if (!element._init.includes(instance.$name)) {
              instance.$setup(element);
              element._init.push(instance.$name);
            }
          });
        }
      });
      document.addEventListener(
        "DOMContentLoaded",
        Initializer[instance.$name]
      );
    }
  }

  #initAll() {
    for (let method in Initializer) {
      Initializer[method]();
    }
  }

  #observe() {
    const config = { childList: true, subtree: true };
    const init = (ml, ob) => {
      for (let m of ml) {
        if (m.type == "childList") {
          this.#initAll();
        }
      }
    };
    const debounce = () => {
      let timeoutId;
      return function(...args) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => init.apply(this, args), 500);
      };
    };
    const observer = new MutationObserver(debounce());
    observer.observe(Initializer.#HTML, config);
  }

  constructor() {
    if (Initializer.#HTML.classList.contains("kentico-page-builder"))
      this.#observe();
    return this.#addMethod;
  }
}
