import Initializer from "./_initializer";
import * as utils from "../utils";
import * as plugins from "../plugins";

export default class Blue {
  static #initializer = new Initializer();

  #name = undefined;
  #selector = undefined;
  #setup = undefined;

  #generateNameString() {
    return (
      Math.random()
        .toString(36)
        .substring(2, 15) +
      Math.random()
        .toString(36)
        .substring(2, 15)
    );
  }

  #checkUsage() {
    if (typeof this.#name !== "string") {
      console.error(
        "Blue error: 'name' is a required parameter. The value must be a unique string."
      );
      return false;
    }
    if (typeof this.#selector !== "string") {
      console.error(
        "Blue error: 'selector' is a required parameter. The value must be valid CSS selector."
      );
      return false;
    }
    if (typeof this.#setup !== "function") {
      console.error(
        "Blue error: 'name' is a required parameter. The value must be a setup function."
      );
      return false;
    }
    return true;
  }

  #createDefaultUtilities() {
    Object.entries(utils).forEach(entry => (this[`$${entry[0]}`] = entry[1]));
    Object.entries(plugins).forEach(entry => (this[`$${entry[0]}`] = entry[1]));
  }

  #applyRequiredParameters({ name, selector, setup }) {
    this.#name = name || this.#generateNameString();
    this.#selector = selector;
    this.#setup = setup;
  }

	get $_this() {
		return this;
	}

  get $name() {
    return this.#name;
  }

  get $selector() {
    return this.#selector;
  }

  get $elements() {
    return [...document.querySelectorAll(this.$selector)];
  }

  get $setup() {
    return this.#setup;
  }

  constructor(params, utils = true) {
		if (utils) {
			this.#createDefaultUtilities();
		}
    this.#applyRequiredParameters(params);

    if (!this.#checkUsage()) return;

    Blue.#initializer(this);

    return this;
  }
}
