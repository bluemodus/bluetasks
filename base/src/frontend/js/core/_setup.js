import Initializer from "./_initializer";
import Element from "./_element";

export default function() {
  const doc = getComputedStyle(document.documentElement);
  const breakpoints = [
    ["XL", parseInt(doc.getPropertyValue("--xl"))],
    ["LG", parseInt(doc.getPropertyValue("--lg"))],
    ["MD", parseInt(doc.getPropertyValue("--md"))],
    ["SM", parseInt(doc.getPropertyValue("--sm"))],
    ["XS", parseInt(doc.getPropertyValue("--xs"))]
  ];

  breakpoints.sort((a, b) => (a[1] > b[1] ? -1 : a[1] < b[1] ? 1 : 0));

  const resize = new ResizeObserver(entries => {
    entries.forEach(entry => {
      breakpoints.some(bp => {
        let comp = entry.target.__comp;
        let width = entry.contentRect.width;
        let current = entry.target.dataset.size;

        if (width >= bp[1]) {
          if (current !== bp[0]) {
            comp.setAttribute("size", bp[0]);
          }
          return true;
        }
        return false;
      });
    });
  });

  const intersection = new IntersectionObserver(
    entries => {
      entries.forEach(entry => {
        let comp = entry.target.__comp;
        comp.setAttribute("on-screen", entry.isIntersecting);
        comp.setAttribute(
          "visible-ratio",
          (Math.round(entry.intersectionRatio * 4) / 4).toFixed(2)
        );
      });
    },
    {
      threshold: [0, 0.25, 0.5, 0.75, 1]
    }
  );

  class Component extends Element {
    connectedCallback() {
      if (this.hasAttribute('observe')) {
        resize.observe(this);
        intersection.observe(this);
      }
    }
    constructor() {
      super();
    }
  }

  self.customElements.define("blue-composition", Element);
  self.customElements.define("blue-component", Component);

  self.Initializer = Initializer;
}
