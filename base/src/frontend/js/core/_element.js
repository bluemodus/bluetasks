import { createElement } from "../utils";

export default class extends HTMLElement {
  #RootStyles = `
	:host {
		display: block;
	}
	:host *,
	:host *::before,
	:host *::after {
		box-sizing: inherit;
  }`;

  #Create() {
    const slot = createElement({
      tag: "slot",
      attributes: { name: "content" }
    });
    const styles = createElement({ tag: "style", text: this.#RootStyles });
    const content = createElement({
      tag: "span",
      attributes: { slot: "content" },
      html: [...this.children]
    });
    const shadow = this.attachShadow({ mode: "open" });

    shadow.appendChild(styles);
    shadow.appendChild(slot);
    this.appendChild(content);
    this.__comp = content.querySelector(':scope > *')
  }
  
  constructor() {
    super();
    this.#Create()
  }
}
