/*-- import aliases -----------
'!/...' -> project root
'~/...' -> js folder root
-----------------------------*/
import { setup } from "~/core";
setup();

import "~/global/_body";
import "~/global/_header";
import "~/global/_footer";

import "~/components/_accordion";
import "~/components/_forms";
