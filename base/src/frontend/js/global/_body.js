import Blue from "~/core";

new Blue({
	name: 'Keyboard Control Outline',
	selector: "body",
	setup(body) {
    this.$addEvents(body, 'mousedown', () => body.classList.add('using-mouse'));
    this.$addEvents(body, 'keydown', () => body.classList.remove('using-mouse'));
	}
});
