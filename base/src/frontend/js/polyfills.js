import { ResizeObserver as ResizeObserverPolyfill } from "@juggle/resize-observer";
// iOS13 doesn't like ResizeObservers
if (!window.ResizeObserver) {
  window.ResizeObserver = ResizeObserverPolyfill;
}


/*****
 *  UNCOMMENT BELOW IF YOU NEED IE11 SUPPORT *
*****/

// import 'element-closest-polyfill';
// import 'element-qsa-scope'
// import 'focus-within-polyfill'
// import 'intersection-observer'
// import 'web-animations-js'
// import 'whatwg-fetch'