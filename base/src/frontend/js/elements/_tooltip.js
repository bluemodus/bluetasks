// https://medium.com/carwow-product-engineering/building-a-simple-tooltip-component-that-never-goes-off-screen-c7039dcab5f9
import {Element} from "~/core";

class Tooltip extends Element {
  connectedCallback() {
    this.setup()
  }
  setup() {
  }
}

self.customElements.define("blue-tooltip", Tooltip);