/*
This file should only contain the code for the plugin, not any specific instance of it being used. Please put your code for validate in the file for the component/widget (forms?) you are adding form validation to.
USAGE:
	import validate from '~/utils/validate'

	const form = document.querySelector('.theFormIWantToValidate')
	validate(form)

PUG:
	//this project is setup to allow the following usage by default, however this can be changed or expanded upon in components/forms.js
	form.formValidate
		input(type="text", required)
		input(type="text", required)
		input(type="text", required)
		input(type="text", required)
		button(type="submit") Submit
*/

import { escapeRegExp } from "lodash-es";
window.escapeRegExp = escapeRegExp;

export default function(form) {
	const inputs = [...form.querySelectorAll('input, textarea, select')];
	const submit =
		form.querySelector('input[type="submit"]') ||
		form.querySelector('button[type="submit"]') ||
		form.querySelector('[class*="submit"]');
	const error = message => `<div class="formError">${message}</div>`;
	/* 
        Function to reset validation errors then check for validity using HTML5 validation methods.
        This function is triggered but the 'blur' event on a form element.
        The function also removes, then re-adds the event handler for the 'input' event,
        so that the element is only validated on 'input' after it has been blurred at least once.
        This gives the user the chance to enter valid data before the validation starts. 
    */
	const validate = event => {
		const input = event.target;
		let group;
		// This is to handle removing error messages on radio buttons
		if (input.getAttribute('type') == 'radio' && input.getAttribute('name') != null) {
			group = [...form.querySelectorAll(`[name="${input.getAttribute('name')}"]`)];
			group.forEach(radio => {
				radio.setCustomValidity('');
				radio.removeEventListener('change', validate);
				radio.parentNode.classList.remove('is-errored');
				radio.classList.remove('is-invalid');
				// Remove the error div if it exists from other radio buttons
				if (radio.parentNode.querySelectorAll('.formError').length) {
					radio.parentNode.removeChild(radio.parentNode.querySelector('.formError'));
				}
				radio.addEventListener('change', validate);
				radio.checkValidity();
			});
		}
		// Handle situation when there is a group of checkboxes and at least one option is required, but multiple are allowed
		else if (input.getAttribute('type') == 'checkbox' && input.getAttribute('name') != null) {
			let groupValid = false;
			group = [...form.querySelectorAll(`[name="${input.getAttribute('name')}"]`)];
			group.forEach(check => {
				check.setCustomValidity(''); // reset custom validation error to initial (nothing)
				check.required = check.__required; // set required to the initial required value
				groupValid = check.checkValidity() ? true : groupValid; // determine if at least one of the items in the group are selected (i.e. minimum requirement for group)
			});
			group.forEach(check => {
				check.removeEventListener('change', validate);
				check.parentNode.classList.remove('is-errored');
				check.classList.remove('is-invalid');
				// Remove the error div if it exists from other checkboxes
				if (check.parentNode.querySelectorAll('.formError').length) {
					check.parentNode.removeChild(check.parentNode.querySelector('.formError'));
				}
				//if at least one of the checkboxes in this group are checked, remove the required attribute
				if (groupValid) {
					check.required = false;
				} else if (check.getAttribute('title')) {
					check.setCustomValidity(check.getAttribute('title'));
				}
				check.addEventListener('change', validate);
				check.checkValidity();
			});
		}
		// This is to handle all other form elements
		else {
			input.setCustomValidity('');
			input.removeEventListener('input', validate);
			input.parentNode.classList.remove('is-errored');
			input.classList.remove('is-invalid');
			// Remove the error div if it exists
			if (input.parentNode.querySelectorAll('.formError').length) {
				input.parentNode.removeChild(input.parentNode.querySelector('.formError'));
			}
			input.addEventListener('input', validate);
			input.checkValidity();
		}
	};
	/*
        This function is triggered by the 'invalid' event. On 'blur', or on 'input', the element is
        checked for validity (el.checkValidity()). If the element is invalid (per HTML5 validation), 
        the element triggers the 'invalid' event.
    */
	const invalidate = event => {
		const input = event.target;
		input.classList.add('is-invalid');
		input.parentNode.classList.add('is-errored');
		// Remove the error div if it exists so we don't get duplicates
		if (input.parentNode.querySelectorAll('.formError').length) {
			input.parentNode.removeChild(input.parentNode.querySelector('.formError'));
		}
		if (input.value.length && input.getAttribute('pattern')) {
			// Add the error div
			input.setCustomValidity(input.getAttribute('title'));
		}
		input.parentNode.insertAdjacentHTML('beforeend', error(input.validationMessage));
		event.preventDefault();
	};
	/*
        If the submit button is clicked or the form is submitted (enter pressed), check each button for
        validity. An invalid input will trigger its 'invalid' event handler. If any invalid inputs are
        found, it will preventDefault() on the form.
    */
	const doSubmit = event => {
		let valid = true;
		inputs.forEach(input => {
			// Trigger a blur to bind to the input event
			input.focus();
			input.blur();
			if (!input.checkValidity()) {
				valid = false;
			}
		});
		if (!valid) {
			event.preventDefault();
		}
	};

	const doValidate = () =>{
		let valid = true;
		let top = window.scrollY
		inputs.forEach(input => {
			// Trigger a blur to bind to the input event
			input.focus();
			input.blur();
			if (!input.checkValidity()) {
				valid = false;
			}
		});
		window.scroll(0, top)
		return valid;
	}
	/*
        Loop over each input and bind the above functions to the 'blur', and 'invalid' events.
    */
	inputs.forEach(input => {
		input.__required = input.required;
		input.addEventListener('blur', validate);
		input.addEventListener('invalid', invalidate);
	});
	/*
       	Attach do submit to form and submit button
    */
	submit.addEventListener('click', doSubmit);
	form.addEventListener('submit', doSubmit);
	form.__doValidate = doValidate
};