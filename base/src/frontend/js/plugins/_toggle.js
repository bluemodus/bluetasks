export default class {
	#id = "data-"; //unique data id applied to the trigger and content for scoping css
	#properties = { //properties to toggle (default from 0 to auto)
		default: true,
		props: ["height"]
	};
	#timing = { //animation timing properties
		duration: 350,
		easing: "ease"
	};
	#scrollTest = () => false; // function (or bool) used to determine if we should scroll to the content on toggle
	#log_error() {
		throw "ToggleAnything: Invalid markup hierarchy. The content should be a direct sibling, or a direct child of the trigger only.";
	}
	#generate_id() {
		this.#id += Math.random()
			.toString(36)
			.split("")
			.filter((value, index, self) => {
				return self.indexOf(value) === index;
			})
			.join("")
			.substr(2, 8);
	}
	#make_styles() {
		const style = document.createElement("style");
		style.textContent = `
			[${this.#id}] {
				overflow: hidden;
				height: 0;
			}
			[${this.#id}][data-active] {
				height: auto;
			}`;
		document.body.insertAdjacentElement("afterbegin", style);
	}
	#process_triggers() {
		this.trigger.forEach((t) =>
			t.addEventListener("click", () => this.toggle(t))
		);
	}
	#process_contents() {
		this.content.forEach((c) => c.setAttribute(this.#id, ""));
	}
	#initialize() {
		this.#generate_id();
		this.#properties.default ? this.#make_styles() : false;
		this.#process_triggers();
		this.#process_contents();
	}
	#get_property_values(c) {
		const styles = getComputedStyle(c);
		return this.#properties.props.reduce((values, prop) => {
			values[prop] = styles[prop];
			return values;
		}, {});
	}
	#get_offset() {
		return this.offset.reduce(
			(offset, o) => ((offset += o.offsetHeight), offset),
			0
		);
	}
	#animate(el, keyframes) {
		return el.animate(keyframes, this.#timing);
	}
	#scroll(t) {
		const top = t.getBoundingClientRect().top - this.#get_offset();
		scrollBy({ top, behavior: "smooth" });
	}
	//Public Methods
	toggle = async (t) => {
		const keyframes = [];
		const do_toggle = (c) => {
			if (c === t.nextElementSibling || t === c.parentElement) {
				keyframes.push(this.#get_property_values(c));
				t.toggleAttribute("data-active");
				c.toggleAttribute("data-active");
				keyframes.push(this.#get_property_values(c));
				this.#animate(c, keyframes).onfinish;
				this.#scrollTest() ? this.#scroll(t) : false;
				return true;
			}
			return false;
		};
		await this.close(t);
		if (!this.content.some(do_toggle)) this.#log_error();
	};
	close(t = false) {
		return new Promise((resolve) => {
			const keyframes = [];
			const s = this.trigger.find((s) => s !== t && s.hasAttribute("data-active"));
			const do_close = () => {
				let c = s.querySelector(this.#id) || s.nextElementSibling;
				keyframes.push({
					height: `${c.offsetHeight}px`,
					width: `${c.offsetWidth}px`
				});
				s.removeAttribute("data-active");
				c.removeAttribute("data-active");
				keyframes.push({
					height: `${c.offsetHeight}px`,
					width: `${c.offsetWidth}px`
				});
				this.#animate(c, keyframes).onfinish = resolve;
			};
			s ? do_close() : resolve();
		});
	}
	constructor({ trigger, content, offset, scrollTest, timing, props }) {
		this.trigger =
			typeof trigger === "string"
				? [...document.querySelectorAll(trigger)]
				: typeof trigger[Symbol.iterator] === 'function'
				? [...trigger]
				: [trigger];
		this.content =
			typeof content === "string"
				? [...document.querySelectorAll(content)]
				: typeof content[Symbol.iterator] === 'function'
				? [...content]
				: [content];
		this.offset = offset
			? typeof offset === "string"
				? [...document.querySelectorAll(offset)]
				: typeof offset[Symbol.iterator] === 'function'
				? [...offset]
				: [offset]
			: [];
		timing ? (this.#timing = timing) : false;
		props
			? ((this.#properties.props = props), (this.#properties.default = false))
			: false;
		scrollTest ? (this.#scrollTest = scrollTest) : false;
		this.#initialize();
		return this;
	}
}
