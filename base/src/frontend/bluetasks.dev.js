/*
  BlueTasks Config for FE Development
*/
module.exports = {
  client: '[client name]',
  node_env: 'development',
  local: true,
  tasks: [
    'clean',
    'copy',
    'pug',
    'sass',
    'script',
    'script_legacy',
    'vue',
    'server',
  ],
};
