# @blue-modus/bluetasks
## Front-End Task Runner
```sh
	npm install --save-dev @blue-modus/bluetasks
```

### Example Scripts:
 ***...in your package.json***

1) Optionally pass a `--port` flag to a task that runs the server to specify the port it should run on. If this is omitted, the server will generate a random port.
2) Optionally pass a `--config` flag to a task. This should be a relative path to a configuration file. Use this file to override the default any of the default options listed below.
```javascript
	"dev": "bluetasks --port=3000 --config=./bluetasks.dev.js",
```
```javascript
	"dist": "bluetasks --config=./bluetasks.dist.js"
```

### Available Options:
 ***...and their defaults***
```javascript
	module.exports = {
		bwd: __dirname.replace('\\', '\\\\'),
		pwd: process.cwd().replace('\\', '\\\\'),
		client: '[client website]',
		verbose: true,
		debounce_add: 2000,
		debounce_change: 500,
		source: './',
		output: './dist/',
		tasks: [
			'clean',
			'copy',
			'pug',
			'sass',
			'script',
			'vue',
			// 'mjml',
			// 'optimize',
			'server'
		],
		clean: {
			title: 'Clean Output',
			script: './task/clean.js',
			source: '',
			output: '',
			allows: [
				'${this.pug.output}*.html',
				'${this.mjml.output}*.html',
				'${this.sass.output}*.css',
				'${this.sass.output}*.map',
				'${this.script.output}*.js',
				'${this.script.output}*.map',
				'${this.vue.output}*.js',
				'${this.vue.output}*.map',
				'${this.vue.output}*.css',
				'${this.copy.allows[0]}',
				'${this.copy.allows[1]}',
				'${this.copy.allows[2]}'
			],
			options: {
				allows: {}
			}
		},
		copy: {
			title: 'Copy Assets',
			script: './task/copy.js',
			source: '',
			output: '',
			allows: [
				'./img/**/*.{png,jpg,jpeg,gif,bmp,svg}',
				'./fonts/**/*.{ttf,otf,woff,woff2,svg,eot}',
				'./robots.txt'
			],
			watch: true,
			options: {
				allows: {}
			}
		},
		optimize: {
			title: 'Optimize Images',
			script: './task/optimize.js',
			source: '',
			output: '',
			allows: './img/**/*.{png,jpg,jpeg}',
			options: {
				allows: {}
			}
		},
		pug: {
			title: 'Compile Pug',
			script: './task/pug.js',
			source: './pug/',
			output: './',
			allows: '**/*.{pug,jade}',
			watch: true,
			options: {
				allows: {},
				pretty: true
			},
			globals: {},
		},
		mjml: {
			title: 'Compile MJML',
			script: './task/mjml.js',
			source: './mjml/',
			output: './email/',
			allows: '*.{pug,jade,mjml}',
			watch: true,
			options: {
				allows: {}
			},
			globals: {},
		},
		sass: {
			title: 'Compile Sass',
			script: './task/sass.js',
			source: './scss/',
			output: './styles/',
			allows: '*.{sass,scss}',
			watch: true,
			prefix: {},
			options: {
				uglify: false,
				allows: {}
			},
		},
		vue: {
			title: 'Compile Vue',
			script: './task/vue.js',
			source: './vue/',
			output: './vue/',
			allows: '*.{js,json,vue,css,scss}',
			watch: true,
			options: {
				uglify: false,
				scss: ['src/assets/styles/main'],
				allows: {}
			},
		},
		script: {
			title: 'Compile Script',
			script: './task/script.js',
			source: './js/',
			output: './js/',
			allows: '*.js',
			watch: true,
			options: {
				uglify: false,
				allows: {}
			},
		},
		server: {
			title: 'Dev Server',
			script: './task/server.js',
		},
		browsers: [
			'last 2 versions',
			'ie >= 11'
		]
	}

```