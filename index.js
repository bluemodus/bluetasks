#!/usr/bin/env node

const _ = require('lodash');
const Config = require('./core/config');

class BlueTasks {
  init() {
    // get bluetasks config
    this.config = new Config().init().bootstrap();
    // create a sequence chain for tasks
    this.tasks = _.chain(this.config.get('tasks', []));
    // return for chaining
    return this;
  }

  sequence() {
    // return next task or done
    return this.tasks.next();
  }

  async execute() {
    // get next task from running
    const next = this.sequence();
    // if not done run the next task
    if (!next.done) {
      const exec = require(next.value._computed.script);
      const task = new exec(next.value, this.config);
      await task.execute().catch(() => false);
      setTimeout(() => {
        this.execute();
      }, 50);
    }
  }
}

new BlueTasks().init().execute();
