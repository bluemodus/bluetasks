const sass = require('sass');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');
const Task = require('../core/task');

class SassTask extends Task {
	execute() {
		return new Promise(async (resolve, reject) => {
			//clear out list of compiled files at start of new execute
			this.compilation = [];
			// log task title
			await this.logStart();
			// glob computed allows
			await this.files();
			// execute compile function and resolve task
			this.compile()
				.then(resolve)
				.catch(reject);
		});
	}

	compile() {
		return new Promise((resolve, reject) => {
			const _do = async () => {
				const next = this.sequence();
				if (!next.done) {
					const source = next.value;
					const output = source
						.replace(this.task._computed.source, this.task._computed.output)
						.replace(/\.sass$|\.scss$/i, '.css');
					try {
						const options = {
							data: await this.fs.readFile(source, { encoding: 'utf8', flag: 'r' }),
							file: source,
							includePaths: [this.task._computed.source.slice(0, -1)],
							outFile: output,
							outputStyle: this.task.options.minify ? 'compressed' : 'expanded',
							sourceMap: true,
							sourceMapRoot: this.task._computed.source,
							sourceMapContents: true,
						};
						const autoprefix = {
							grid: true,
							overrideBrowserslist: this.config.get('browsers', []),
							...(this.task.prefix || {}),
						};
						const render = sass.renderSync(options);
						const post = await postcss([autoprefixer(autoprefix)]).process(render.css, {
							from: source,
							to: output,
						});
						await this.fs.ensureDir(this.path.dirname(output));
						await this.fs.writeFile(output, post.css);
						await this.logSuccess(source, output);
						if (render.map) {
							await this.fs.writeFile(`${output}.map`, render.map);
							await this.logSuccess(source, `${output}.map`);
						}
						this.compilation.push(output);

						_do();
					} catch (error) {
						await this.logFailure(source, output);
						await this.logError(error);
						resolve(false);
					}
				} else {
					await this.logFinish();
					resolve(true);
				}
			};
			_do();
		});
	}
}

module.exports = SassTask;