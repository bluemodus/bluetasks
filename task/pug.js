const pug = require('pug');
const ModifyAttributes = require('./plugins/modify-attributes')
const Task = require('../core/task');

class PugTask extends Task {
  execute() {
    return new Promise(async (resolve, reject) => {
      //clear out list of compiled files at start of new execute
      this.compilation = [];
      // log task title
      await this.logStart();
      // glob computed allows
      await this.files();
      // execute compile function and resolve task
      this.compile().then(resolve).catch(reject);
    });
  }

  compile() {
    return new Promise(async (resolve, reject) => {
			let content = {};
			if (this.task._computed.hasOwnProperty('content')) {
				try {
					content = await this.fetch();
				} catch (error) {
					await this.logFailure(source, output);
					await this.logError(error);
					reject(false);
				}
			}
      const _do = async () => {
        const next = this.sequence();
        let source;
				let output;
				
        if (!next.done) {
          source = this.source = next.value;
          output = this.output = source
            .replace(this.task._computed.source, this.task._computed.output)
						.replace(/\.pug$|\.jade$/i, '.html');
							
          try {
            const render = pug.compileFile(source, { ...this.task.options, plugins: [ModifyAttributes] })({ ...content });
            await this.fs.ensureDir(this.path.dirname(output));
            await this.fs.writeFile(output, render);
            await this.logSuccess(source, output);
            this.compilation.push(output);
            _do();
          } catch (error) {
            await this.logFailure(source, output);
            await this.logError(error);
            reject(false);
          }
        } else {
          await this.logFinish();
          resolve(true);
        }
      };
      _do();
    });
  }
}

module.exports = PugTask;
