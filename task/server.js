const bs = require('browser-sync');
const Task = require('../core/task');

class ServerTask extends Task {
  execute() {
    return new Promise(async (resolve) => {
      const urls = {};
      const port = this.task.port || Math.floor(Math.random() * (49150 - 1024 + 1)) + 1024;
      const server = bs.create(this.task.title);

      server.use({
        plugin() { }
      });

      server.init({
        port,
        notify: false,
        server: {
          baseDir: this.config.get('output'),
          directory: false,
          serveStaticOptions: {
            sourceMap: true,
            extensions: ['html'],
          },
        },
        logLevel: 'silent',
        reloadDebounce: 1000,
        ...this.task.options || {},
      }, (error, instance) => {
        if (!error) {
          const info = [...instance.options.get('urls')];
          info.map((value) => urls[value[0]] = value[1]);
          this.logServer(urls);
        } else {
          this.logError(error);
        }
      });

      const tasks = this.config.get('tasks', []).filter((task) => task && this.util.bool.truthy(task.watch));

      tasks.forEach(async (watch) => {
        if (watch.script) {
          try {
            const exec = require(watch._computed.script);
            const task = new exec(watch, this.config);
            await task.watch(server, urls, true);
          } catch (error) {
            this.logError(error);
          }
        }
      });
      // resolve task
      resolve(true);
    });
  }
}

module.exports = ServerTask;
