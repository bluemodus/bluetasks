const del = require('del');
const Task = require('../core/task');

class CleanTask extends Task {
  execute() {
    return new Promise(async (resolve) => {
      // replace source with output on allows paths
      this.task._computed.allows.forEach((path, i) => {
        this.task._computed.allows[i] = path.replace(this.task._computed.source, this.task._computed.output);
      });
      // delete files matching allows patterns from output folder
      const files = await del(this.task._computed.allows);
      // log task data
      if (files && files.length) {
        this.logStart();
        files.forEach(async (output, i) => {
          await this.logFailure(null, output, false);
          if (i === files.length - 1) await this.logFinish();
        });
      }
      // resolve task
      resolve(true);
    });
  }
}

module.exports = CleanTask;
