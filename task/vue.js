const rollup = require('rollup');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const alias = require('@rollup/plugin-alias');
const importResolver = require('rollup-plugin-import-resolver');
const babel = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const globals = require('rollup-plugin-node-globals');
const builtins = require('rollup-plugin-node-builtins');
const css = require('rollup-plugin-css-only');
const postcss = require('postcss');
const autoprefixer = require('autoprefixer');
const vue = require('rollup-plugin-vue');
const json = require('@rollup/plugin-json');
const { terser } = require('rollup-plugin-terser');
const Task = require('../core/task');

class VueTask extends Task {
  execute(is_server = false) {
    return new Promise(async (resolve, reject) => {
      //holder for rollup watchers - used for logging to know if the current watcher is the first or the last
      this.watchers = [];
      // log task title
      if (!is_server) await this.logStart();
      // glob computed allows
      await this.files();
      // execute compile function and resolve task
      this.compile(is_server).then(resolve).catch(reject);
    });
  }

  compile(is_server) {
    let is_silent = true;
    let resolve_initial;
    return new Promise((resolve, reject) => {
      const _do = async () => {
        const next = this.sequence();
        if (!next.done) {
          const source = next.value;
          const output = source.replace(
            this.task._computed.source,
            this.task._computed.output
          );
          let hasCSS = false;
          let output_css, output_js;

          if (this.task._computed.hasOwnProperty('output_css')) {
            output_css = source.replace(
              this.task._computed.source,
              this.task._computed.output_css
            );
          } else output_css = output;
          if (this.task._computed.hasOwnProperty('output_js')) {
            output_js = source.replace(
              this.task._computed.source,
              this.task._computed.output_js
            );
          } else output_js = output;

          try {
            const inputOptions = {
              input: source,
              context: 'window',
              ...this.task.options.rollup.input,
              plugins: [
                nodeResolve({
                  browser: true,
                  ...this.task.options.rollup.resolve,
                }),
                importResolver({
                  extensions: ['.js', '.ts', '.css', '.scss', '.vue'],
                }),
                alias({
                  resolve: ['.js', '.ts', '.css', '.scss', '.vue'],
                  entries: [
                    {
                      find: '~',
                      replacement: this.task._computed.source.slice(0, -1),
                    },
                    {
                      find: '@',
                      replacement: `${this.task._computed.source.slice(
                        0,
                        -1
                      )}/src`,
                    },
                    {
                      find: '!',
                      replacement: `${this.task._computed.pwd.slice(0, -1)}`,
                    },
                  ],
                }),
                json(),
                css({
                  output: styles => {
                    const autoprefix = {
                      grid: true,
                      overrideBrowserslist: this.config.get('browsers', []),
                      ...(this.task.prefix || {}),
                    };
                    output_css = output_css.replace(/\.js$|\.ts$/i, '.css');
                    hasCSS = true;

                    postcss([autoprefixer(autoprefix)])
                      .process(styles, {
                        from: source,
                        to: output_css,
                      })
                      .then(async post => {
                        await this.fs.ensureDir(this.path.dirname(output_css));
                        await this.fs.writeFile(output_css, post.css);
                      });
                  },
                }),
                vue({
                  needMap: false,
                  css: false,
                  data: {
                    scss: this.task.options.scss.reduce((_imports, file) => {
                      let path = this.path.normalize(
                        this.path.join(this.task._computed.source, file)
                      );
                      path = path.replace(/\\/g, '/');
                      _imports += `@import '${path}'; `;
                      return _imports;
                    }, "@use 'sass:math';"),
                  },
                }),
                babel.babel({
                  babelHelpers: 'runtime',
                  sourceType: 'unambiguous',
                  extensions: ['.js', '.jsx', '.es6', '.es', '.mjs', '.vue'],
                  plugins: [
                    ['@babel/transform-runtime', { corejs: 3 }],
                    ...this.task.options.babel.plugins,
                  ],
                  presets: [
                    [
                      '@babel/preset-env',
                      { targets: this.config.get('browsers', []) },
                    ],
                  ],
                  exclude: /node_modules/,
                }),
                commonjs(),
                globals(),
                builtins(),
              ],
            };
            const outputOptions = {
              file: output_js,
              format: 'iife',
              name: 'BlueBundle',
              compact: true,
              sourcemap: true,
              ...this.task.options.rollup.output,
            };
            if (this.task.options.rollup.plugins.length) {
              this.task.options.rollup.plugins.forEach(plugin => {
                let plug = require(plugin);
                let hasOptions = false;
                let options;
                if (this.task.options.rollup.pluginOptions.length) {
                  options =
                    this.task.options.rollup.pluginOptions.find(
                      options => options.name === plugin
                    ) || false;
                  if (options) {
                    hasOptions = true;
                    options = options.options;
                  }
                }
                if (hasOptions) {
                  inputOptions.plugins.push(plug(options));
                } else {
                  inputOptions.plugins.push(plug());
                }
              });
            }
            if (this.task.options.minify) {
              inputOptions.plugins.push(terser());
            }
            const watchOptions = {
              ...inputOptions,
              output: outputOptions,
              watch: {
                clearScreen: false,
                include: `${this.task._computed.source}**`,
              },
            };

            const initial_run = new Promise(
              resolve => (resolve_initial = resolve)
            );
            const watcher = rollup.watch(watchOptions);
            this.watchers.push(watcher);
            watcher.on('event', async e => {
              if (is_server && !is_silent) {
                if (e.code === 'START') await this.logStart();
                else if (e.code === 'ERROR') {
                  await this.logError(e.error);
                  await this.logFailure(
                    source,
                    output_js === output_css
                      ? output_js
                      : [output_js, output_css]
                  );
                } else if (e.code === 'END') {
                  this.server.reload(output_js);
                  await this.logSuccess(
                    source,
                    output_js === output_css
                      ? output_js
                      : [output_js, output_css]
                  );
                  await this.logFinish();
                  await this.logServer(this.urls);
                }
              } else if (e.code === 'ERROR' || e.code === 'END') {
                if (!is_server) resolve_initial();
                else if (
                  is_silent &&
                  this.watchers.indexOf(watcher) === this.watchers.length - 1
                )
                  is_silent = false;
              }
              //if initial bundle has an error the watcher breaks, so throw error and exit process
              if ((e.code === 'ERROR' || e.code === 'FATAL') && !is_server) {
                resolve_initial();
                await this.logFailure(
                  source,
                  hasCSS ? [output_js, output_css] : output_js
                );
                await this.logError(e.error);
                process.exit(0);
              }
            });

            if (!is_server) {
              await initial_run;
              await this.logSuccess(
                source,
                hasCSS ? [output_js, output_css] : output_js
              );
              watcher.close();
            }
            this.compilation.push(output_js);
            _do();
          } catch (error) {
            await this.logFailure(
              source,
              hasCSS ? [output_js, output_css] : output_js
            );
            await this.logError(error);
            reject(false);
          }
        } else {
          if (!is_server) await this.logFinish();
          resolve(true);
        }
      };
      _do();
    });
  }

  watch(server, urls, is_server) {
    return new Promise(async resolve => {
      this.server = server;
      this.urls = urls;
      await this.execute(is_server);
      resolve(true);
    });
  }
}

module.exports = VueTask;
