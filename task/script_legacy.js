const UglifyJS = require('uglify-js');
const babel = require('@babel/core');
const Task = require('../core/task');

class ScriptLegacyTask extends Task {
  execute(is_server = false) {
    return new Promise(async (resolve, reject) => {
      //clear out list of compiled files at start of new execute
      this.compilation = [];
      // log task title
      await this.logStart();
      // glob computed allows
      await this.files();
      // execute compile function and resolve task
      this.compile().then(resolve).catch(reject);
    });
  }

  compile(is_server) {
    return new Promise((resolve, reject) => {
      const nameCache = {};
      const _do = async () => {
        const next = this.sequence();
        if (!next.done) {
          const source = next.value;
          const output = source.replace(
            this.task._computed.source,
            this.task._computed.output
          );
          try {
            const options = {
              nameCache,
              compress: false,
            };
            const regex = new RegExp(/\/\/\s?concat:=(.*?)$/, 'gm');
            const sourceString = await this.fs.readFile(source, 'utf8');
            const filePaths = [...sourceString.matchAll(regex)].map(m =>
              this.util.path.abs(this.task._computed.source, m[1])
            );
            const files = {};

            await filePaths.reduce(async (prevPromise, path) => {
              await prevPromise;
              let name = this.path.basename(path);
              files[name] = await this.fs.readFile(path, 'utf8');
              return files[name];
            }, Promise.resolve());

            const uglified = UglifyJS.minify(files, options);
            let code;

            if (this.task.options.useBabel) {
              const transformed = await babel.transformAsync(uglified.code, {
                filename: this.path.basename(source),
                sourceType: 'unambiguous',
                plugins: [
                  '@babel/plugin-proposal-class-properties',
                  '@babel/plugin-proposal-private-methods',
                  ['@babel/transform-runtime', { corejs: 3 }],
                  ...this.task.options.babel.plugins,
                ],
                presets: [
                  [
                    '@babel/preset-env',
                    { targets: this.config.get('browsers', []) },
                  ],
                ],
                exclude: /node_modules/,
                generatorOpts: {
                  minified: this.task.options.minify,
                },
              });

              code = transformed.code;
            } else {
              code = uglified.code;
            }

            await this.fs.ensureDir(this.path.dirname(output));
            await this.fs.writeFile(output, code);
            await this.logSuccess(source, output);
            this.compilation.push(output);
            _do();
          } catch (error) {
            await this.logFailure(source, output);
            await this.logError(error);
            reject(false);
          }
        } else {
          if (!is_server) await this.logFinish();
          resolve(true);
        }
      };
      _do();
    });
  }
}

module.exports = ScriptLegacyTask;
