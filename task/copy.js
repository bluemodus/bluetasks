const Task = require('../core/task');

class CopyTask extends Task {
  execute() {
    return new Promise(async (resolve, reject) => {
      //clear out list of compiled files at start of new execute
      this.compilation = [];
      // log task title
      await this.logStart();
      // glob computed allows
      await this.files();
      // execute compile function and resolve task
      this.compile().then(resolve).catch(reject);
    });
  }

  compile() {
    return new Promise((resolve, reject) => {
      const _do = async () => {
        const next = this.sequence();
        if (!next.done) {
          const source = next.value;
          const output = source.replace(this.task._computed.source, this.task._computed.output);
          try {
            await this.fs.ensureDir(this.path.dirname(output));
            await this.fs.copyFile(source, output);
            await this.logSuccess(source, output);
            this.compilation.push(output);
            _do();
          } catch (error) {
            await this.logFailure(source, output);
            await this.logError(error);
            reject(false);
          }
        } else {
          await this.logFinish();
          resolve(true);
        }
      };
      _do();
    });
  }
}

module.exports = CopyTask;
