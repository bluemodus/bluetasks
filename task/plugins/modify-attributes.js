//example of plugin using this lifecycle hook: https://github.com/pugjs/pug/issues/2474

const mod_attr = {
  _guid() {
    return (
      Math.random().toString(36).substring(2, 15) +
      Math.random().toString(36).substring(2, 15)
    );
  },
  alt(node) {
    node.attrs.push({
      name: 'alt',
      val: `'__alt__${this._guid()}'`,
      mustEscape: false,
    });
  },
  lazy(node) {
    const critical = node.attrs.find(a => a.name === 'critical');
    if (critical) return;
    node.attrs.push({
      name: 'loading',
      val: `'lazy'`,
      mustEscape: false,
    });
  },
  title(node) {
    node.attrs.push({
      name: 'aria-label',
      val: `'__title__${this._guid()}'`,
      mustEscape: false,
    });
  },
};

const dig_deeper = tokens => {
  if (tokens.nodes && tokens.nodes.length) {
    tokens.nodes.forEach(node => {
      switch (node.name) {
        case 'img':
          mod_attr.alt(node);
          mod_attr.lazy(node);
          break;
        case 'a':
          mod_attr.title(node);
          break;
        default:
          break;
      }
      dig_deeper(node);
    });
  }
  if (tokens.block) {
    dig_deeper(tokens.block);
  }
  if (tokens.consequent) {
    dig_deeper(tokens.consequent);
  }
};

const modifyAttributes = {
  postParse(tokens, options) {
    dig_deeper(tokens);
    return tokens;
  },
};

module.exports = modifyAttributes;
