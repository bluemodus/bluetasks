const _ = require('lodash');
const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');
const util = require('./util');
const fetch = require('node-fetch');
const { config } = require('yargs');

class Task {
  constructor(task, config) {
    // scope npm libs for extending class
    this._ = _;
    this.fs = fs;
    this.path = path;
    this.glob = glob;
    this.util = util;
    this.debounce_change = config.get('debounce_change', 500);
    this.debounce_add = config.get('debounce_add', 2000);
    // task start time
    this.time = Date.now();
    // scope task object
    this.task = task;
    // scope bluetasks config class
    this.config = config;
    // scope verbosity setting
    this.verbose = config.get('verbose', false);
    // placeholder for file sequences
    this.tasks = _.chain([]);
    // placeholder for files compiled by tasks
    this.compilation = [];
    // fetch api for loading dynamic content into tasks
    this.fetch = async () => {
      let data = {};
      // if the content path is a local string
      if (this.task._computed.local) {
        // create absolute paths out of content values
        let content = this.task._computed.content.reduce((acc, file) => {
          if (glob.hasMagic(file)) {
            glob.sync(file, {}).forEach(file => acc.push(file));
            return acc;
          }
          acc.push(file);
          return acc;
        }, []);
        // async loop over multiple files and concat data into a single object for use in task
        await content.reduce(async (prevPromise, file) => {
          await prevPromise;
          return new Promise(async resolve => {
            let exists = await fs.pathExists(file);
            if (exists) {
              let { Response } = fetch;
              let stream = fs.createReadStream(file);
              let res;
              try {
                res = await new Response(stream).json();
              } catch (error) {
                this.logError(error);
              }
              if (Array.isArray(res)) {
                this.logError(
                  `The root content in ${file} must be an object.\nArrays are NOT allowed.`
                );
              } else {
                data = { ...data, ...res };
              }
            } else {
              this.logError(`The local file ${file} does not exist.`);
            }
            resolve();
          });
        }, Promise.resolve());
      }
      // if the content path is an external URL try to fetch the content
      else {
        try {
          data = await (await fetch(this.task._computed.content)).json();
        } catch (error) {
          this.logError(error);
        }
      }
      return data;
    };
  }

  files(pattern) {
    return new Promise(async resolve => {
      const files = [];
      let paths = [];
      if (pattern) {
        paths = [...pattern];
      } else {
        paths = [...this.task._computed.allows];
      }
      if (paths.length) {
        paths.forEach(pattern => {
          if (pattern) {
            glob.sync(pattern, this.task._computed.ignores).map(value => {
              files.push(path.normalize(value));
            });
          }
        });
      }
      this.tasks = _.chain(files);
      resolve(files);
    });
  }

  sequence() {
    return this.tasks.next();
  }

  logStart() {
    return new Promise(resolve => {
      this.time = Date.now();
      util.log({
        cast: 'cyan.bold',
        text: util.line(37, this.task.title, false),
        new: true,
      });
      resolve(this);
      return this;
    });
  }

  logFinish() {
    return new Promise(resolve => {
      util.log({
        cast: 'cyan.bold',
        text: util.line(37, `${util.mark(this.time)}s`, true),
      });
      resolve(this);
      return this;
    });
  }

  logServer(urls) {
    return new Promise(resolve => {
      util.log({
        cast: 'cyan.bold',
        text: util.line(37, this.config.get('server.title'), false),
        new: true,
      });
      util.log(
        { cast: 'dim.bold', icon: 'play' },
        { cast: 'bold', text: 'Local:  ' },
        { cast: 'bold.magenta', text: urls.local }
      );
      util.log(
        { cast: 'dim.bold', icon: 'play' },
        { cast: 'bold', text: 'Network:' },
        { cast: 'bold.magenta', text: urls.external }
      );
      util.log({ cast: 'cyan.bold', text: util.line(37) });
      resolve(this);
      return this;
    });
  }

  logError(error, breakOnError = true) {
    return new Promise(resolve => {
      util.log({ cast: 'red.bold', text: util.line(36) });
      util.log({ cast: 'yellow.bold', text: error });
      util.log({ cast: 'red.bold', text: util.line(36) });
      if (!this.config.get('local') && breakOnError) {
        process.exit(1);
      }
      resolve(this);
      return this;
    });
  }

  logSuccess(source, output) {
    return new Promise(resolve => {
      if (!Array.isArray(output)) output = [output];
      util.log(
        { cast: 'green.bold', icon: 'tick' },
        ...output.reduce((acc, out, i) => {
          if (i !== 0) {
            acc.push({ cast: 'dim.bold', icon: 'pointerSmall' });
          }
          acc.push({ cast: 'bold', text: util.path.rel(util.path.pwd, out) });
          return acc;
        }, [])
      );
      if (source) {
        util.log(
          { cast: 'dim.bold', text: 'SOURCE:', pad: 2 },
          { cast: 'dim', text: util.path.rel(util.path.pwd, source) }
        );
      }
      resolve(this);
      return this;
    });
  }

  logFailure(source, output, breakOnError = true) {
    return new Promise(resolve => {
      if (!Array.isArray(output)) output = [output];
      util.log(
        { cast: 'red.bold', icon: 'cross' },
        ...output.reduce((acc, out, i) => {
          if (i !== 0) acc.push({ cast: 'dim.bold', icon: 'pointerSmall' });
          acc.push({
            cast: 'red.bold',
            text: util.path.rel(util.path.pwd, out),
          });
          return acc;
        }, [])
      );
      if (source) {
        util.log(
          { cast: 'dim.bold', text: 'SOURCE:', pad: 2 },
          { cast: 'dim', text: util.path.rel(util.path.pwd, source) }
        );
      }
      if (!this.config.get('local') && breakOnError) {
        process.exit(1);
      }
      resolve(this);
      return this;
    });
  }

  watch(server, urls) {
    return new Promise(async resolve => {
      this.server = server;
      let allowChangeEvent = true;
      let allowAddEvent = true;
      server.watch(
        this.task._computed.watch,
        { ignoreInitial: true },
        async (event, file) => {
          if (event == 'change' && allowChangeEvent) {
            allowChangeEvent = false;
            setTimeout(async () => {
              const success = await this.execute().catch(() => false);
              if (success) {
                server.reload(this.compilation);
                this.logServer(urls);
              }
              allowChangeEvent = true;
            }, this.debounce_change);
          } else if (event == 'add' && allowAddEvent) {
            allowAddEvent = false;
            setTimeout(async () => {
              const success = await this.execute().catch(() => false);
              if (success) {
                server.reload(this.compilation);
                this.logServer(urls);
              }
              allowAddEvent = true;
            }, this.debounce_add);
          }
        }
      );
      // resolve task
      resolve(true);
    });
  }
}

module.exports = Task;
