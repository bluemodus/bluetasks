const _       = require('lodash')
const path    = require('path')
const icons   = require('figures')
const chalk   = require('chalk')
const package = require('../package.json')

module.exports = {
	logo(name) {
		console.log(chalk.cyan.bold(`\n${this.line(37, `v${package.version}`, false)}\n _   _         _____         _       \n| |_| |_ _ ___|_   _|___ ___| |_ ___ \n| . | | | | -_| | | | .\'|_ -| \'_|_ -|\n|___|_|___|___| |_| |__,|___|_,_|___|\n\n${this.line(37, name)}\n`))
	},
	log() {
		const logs = []
		const args = [...arguments]
		if (args && args.length) {
			args.forEach(arg => {
				if (arg instanceof Object) {
					let text = ''
					if (arg.new) {
						text += '\n'
					}
					if (arg.pad) {
						for (let n = 0; n < parseInt(arg.pad); n++) {
							text += ' '
						}
					}
					if (arg.icon) {
						text += icons[arg.icon] || ''
					}
					if (arg.text) {
						text += arg.text
					}
					if (arg.cast) {
						text = _.get(chalk, arg.cast)(text)
					}
					if (text !== '') {
						logs.push(text.trim())
					}
				}
			})
		}
		if (args[0] === false) {
			return logs.join(' ')
		} else {
			console.log(...logs)
		}
		return this
	},
	mark(val) {
		return (Date.now() - val) / 1000
	},
	bool: {
		truthy(val) {
			if (typeof val === 'string') val = val.toLowerCase()
			return [1,true,'on','yes','true'].includes(val)
		},
		falsey(val) {
			if (typeof val === 'string') val = val.toLowerCase()
			return [0,null,false,undefined,'no','off','false'].includes(val)
		},
	},
	path: {
		bwd: path.dirname(__dirname).replace('\\', '\\\\'),
		pwd: process.cwd().replace('\\', '\\\\'),
		abs() {
			const args = [...arguments]
			return path.normalize(path.join(...args))
		},
		rel() {
			const args = [...arguments]
			return path.normalize(path.relative(...args))
		},
	},
	line(len = 37, val = '', end = true) {
		let line = ''
		for (let n = 0; n < len; n++) {
			line+= '-'
		}
		if (val) {
			line = line.substring(1, line.length - val.length)
			return end ? `${line} ${val}` : `${val} ${line}`
		}
		return line
	},
}
