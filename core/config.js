const _ = require('lodash');
const fs = require('fs-extra');
const sro = require('self-referenced-object');
const yargs = require('yargs').argv;
const deepmerge = require('deepmerge');
const validUrl = require('valid-url');
const jsesc = require('jsesc');
const util = require('./util');
const defaults = require('../defaults');

//bwd is working directory of blue tasks (wherever package.json is)
//pwd is working directory of project
const { bwd, pwd } = defaults;

class Config {
  // get a value in the config
  get(key, val = false) {
    if (key) {
      return _.get(this.config, key) || val;
    }
    return this.config;
  }

  // set a value in the config
  set(key, val = false) {
    _.set(this.config, key, val);
  }

  // initialize the config
  init() {
    // config placeholder
    this.config = {};
    // --config=bluetasks.<env>.js (optional)
    const { config } = yargs;
    if (config) {
      // get absolute path of the config
      const absolute = util.path.abs(pwd, config);
      // check that file exists
      if (fs.existsSync(absolute)) {
        try {
          // merge defaults and custom config
          const merge = deepmerge(defaults, require(absolute), {
            // incoming arrays will override any matching array
            arrayMerge: (a, b) => b,
          });

          function charEscape(obj) {
            for (const prop in obj) {
              if (typeof obj[prop] === 'object' && obj[prop] !== null)
                charEscape(obj[prop]);
              else if (
                ['source', 'output', 'pwd', 'bwd', 'script'].includes(prop)
              ) {
                obj[prop] = jsesc(obj[prop]);
              } else if (prop === 'allows') {
                if (Array.isArray(obj[prop])) {
                  obj[prop] = obj[prop].map(item => jsesc(item));
                } else {
                  obj[prop] = jsesc(obj[prop]);
                }
              }
            }
          }

          charEscape(merge);
          // resolve config tokens
          this.config = sro(merge);
        } catch (error) {
          // handle merge|require|sro errors gracefully
          util.log(
            { cast: 'red.bold.underline', text: `\n${util.line(36)}\n` },
            { cast: 'yellow.bold', text: `\n${error.message}\n` },
            { cast: 'red.bold.underline', text: `\n${util.line(36)}\n` }
          );
          process.exit();
        }
      }
      // config doesn't exist notify the user
      else {
        util.log({ cast: 'red.bold', icon: 'cross', text: config });
        util.log({
          cast: 'yellow.italic',
          text: `  File missing! We tried looking for ${absolute}`,
        });
      }
    } else {
      this.config = sro(defaults);
    }
    // --port=3000 (optional)
    let { port } = yargs;
    if (typeof port === 'string' || typeof port === 'number') {
      port = parseInt(port);
      if (port >= 1024 && port <= 49150) {
        _.set(this.config.server, 'port', port);
      }
    }
    // say hello to the user
    util.logo(this.config.client);
    // return for chaining
    return this;
  }

  bootstrap() {
    this.config.tasks.forEach((key, i) => {
      const task = this.config[key];
      const bwdScript = util.path.abs(bwd, task.script);
      const pwdScript = util.path.abs(pwd, task.script);

      _.set(task, '_computed.pwd', util.path.abs(pwd, this.config.source));
      _.set(
        task,
        '_computed.config.source',
        util.path.abs(pwd, this.config.source)
      );
      _.set(
        task,
        '_computed.config.output',
        util.path.abs(pwd, this.config.output)
      );

      // check if script exists in project directory
      if (fs.existsSync(pwdScript)) {
        // create absolute path to the project directory script
        _.set(task, '_computed.script', pwdScript);
      }
      // check if script exists in bluetasks
      else if (fs.existsSync(bwdScript)) {
        // create absolute path to the task script in bluetasks
        _.set(task, '_computed.script', bwdScript);
      }
      // check if the task has an source property
      if (task.hasOwnProperty('source')) {
        // convert source property to an absolute path in the project directory
        _.set(
          task,
          '_computed.source',
          util.path.abs(pwd, this.config.source, task.source)
        );
      }
      // check if the task has an output property
      if (task.hasOwnProperty('output')) {
        // convert source property to an absolute path in the project directory
        _.set(
          task,
          '_computed.output',
          util.path.abs(pwd, this.config.output, task.output)
        );

        //next two if's are specific to Vue task. Allows you to specify different paths for js/css
        if (task.hasOwnProperty('output_css')) {
          _.set(
            task,
            '_computed.output_css',
            util.path.abs(pwd, this.config.output, task.output, task.output_css)
          );
        }
        if (task.hasOwnProperty('output_js')) {
          _.set(
            task,
            '_computed.output_js',
            util.path.abs(pwd, this.config.output, task.output, task.output_js)
          );
        }
      }
      // check if the task has an allows property
      if (task.hasOwnProperty('allows')) {
        // get allows property from the task
        const { allows } = task;
        // check if allows property is an array or string (flatten out nested arrays (clean task looks at entire copy array))
        const patterns = [].concat(
          ...[...(Array.isArray(allows) ? allows : [allows])]
        );
        // placeholder for watch patterns
        const watchers = [];
        // loop over each allows statement
        patterns.forEach((pattern, i) => {
          // convert each allows pattern to an absolute path in the project directory
          patterns[i] = util.path.abs(
            pwd,
            this.config.source,
            task.source,
            pattern
          );
          // check if the task has true for the watch property
          if (task.watch) {
            // convert each allows pattern to an absolute path in the project directory
            if (task.source.trim() == '' || task.source == this.config.source) {
              watchers[i] = util.path.abs(
                pwd,
                this.config.source,
                task.source,
                pattern
              );
            } else {
              watchers[i] = util.path.abs(
                pwd,
                this.config.source,
                task.source,
                '**',
                pattern
              );
            }
          }
        });
        if (task.hasOwnProperty('ignores')) {
          // get ignores property from the task
          const { ignores } = task;
          // check if ignores property is an array or string (flatten out nested arrays (clean task looks at entire copy array))
          const negations = [].concat(
            ...[...(Array.isArray(ignores) ? ignores : [ignores])]
          );
          // placeholder for ignore patterns
          const disregards = [];
          // loop over each ignores statement
          negations.forEach((ignore, i) => {
            if (ignore.length) {
              // convert each ignore to an absolute path in the project directory
              disregards[i] = util.path.abs(
                pwd,
                this.config.source,
                task.source,
                ignore
              );
              // check if the task has true for the watch property
              if (task.watch) {
                // convert each allows ignore to an absolute path in the project directory
                if (
                  task.source.trim() == '' ||
                  task.source == this.config.source
                ) {
                  disregards[i] = util.path.abs(
                    pwd,
                    this.config.source,
                    task.source,
                    ignore
                  );
                } else {
                  disregards[i] = util.path.abs(
                    pwd,
                    this.config.source,
                    task.source,
                    '**',
                    ignore
                  );
                }
              }
            }
          });

          _.set(task, '_computed.ignores', { ignore: disregards });
        }

        watchers.push(`!(${this.config.output})`);
        _.set(task, '_computed.allows', patterns);
        if (task.watch) {
          _.set(
            task,
            '_computed.watch',
            watchers.map(path => path.replace(/\\/gi, '/'))
          );
        }
      }
      // check if the task has a content property
      if (task.hasOwnProperty('content')) {
        // check if path is relative or absolute
        let isArray = Array.isArray(task.content);
        if (!isArray && validUrl.isUri(task.content)) {
          // set already absolute path to the computed content property
          _.set(task, '_computed.content', task.content);
          _.set(task, '_computed.local', false);
        } else {
          let paths = [];
          // convert relative content property to an absolute path in the project directory
          if (isArray) {
            paths = task.content.map(path => {
              return util.path.abs(pwd, this.config.source, path);
            });
          } else {
            paths = [util.path.abs(pwd, this.config.source, task.content)];
          }
          _.set(task, '_computed.content', paths);
          _.set(task, '_computed.local', true);
          if (task.watch) {
            task._computed.watch.push(...paths);
          }
        }
      }
      this.config.tasks[i] = task;
    });
    return this;
  }
}

module.exports = Config;
